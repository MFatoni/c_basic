#include <stdio.h>
#include <conio.h>

int main(){
    int a,b,c,awal,akhir;
    printf("Awal  : ");
    scanf("%d", &awal); 
    printf("Akhir : ");
    scanf("%d", &akhir);
    puts("\nBilangan Ganjil");
    for(a=awal;a<=akhir;a++){
        if(a%2==1){
            printf("%d,",a);
        }
    }
    puts("\nBilangan Genap");
    for(a=awal;a<=akhir;a++){
        if(a%2==0){
            printf("%d,",a);
        }
    }
    puts("\nBilangan Prima");
    for(a=awal;a<=akhir;a++){
        c=0;
        for(b=1;b<=a;b++){
            if(a%b==0){
                c=c+1;
            }
        }
        if(c==2){
            printf("%d,",a);
        }
    }
    return 0;
}